/* vim: set ts=8 sw=8 noexpandtab: */

/* The CSS Theme Engine for Gtk+.
 * Copyright (C) 2008 Robert Staudinger
 *
 * This  library is free  software; you can  redistribute it and/or
 * modify it  under  the terms  of the  GNU Lesser  General  Public
 * License  as published  by the Free  Software  Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed  in the hope that it will be useful,
 * but  WITHOUT ANY WARRANTY; without even  the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License  along  with  this library;  if not,  write to  the Free
 * Software Foundation, Inc., 51  Franklin St, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 */

#include <gtk/gtk.h>
#include "gce-maps.h"
#include "gce-node.h"
#include "gce-style.h"
#include "config.h"

static GType          gce_style_type = 0;
static GtkStyleClass *gce_style_parent_class = NULL;

static GHashTable *_widget_aux_hash = NULL;

/* TODO gtk_widget_queue_draw() */
static void
has_toplevel_focus_notify (GtkWindow	*window,
			   GParamSpec	*pspec,
			   GtkWidget	*widget)
{
	gtk_widget_queue_draw_area (widget, 0, 0, G_MAXINT, G_MAXINT);
}

/* TODO use "unrealized" instead. */
static void
widget_destroy_cb (GtkWidget	 *widget,
		   GtkWidget	 *watchee)
{
	gulong id;

	id = g_hash_table_steal (_widget_aux_hash, widget);

	if (g_signal_handler_is_connected (watchee, id))
		g_signal_handler_disconnect (watchee, id);
}

static void
rectangle (GceStyle		*self, 
	   ccss_node_t 		*node,
	   GdkWindow		*window, 
	   GdkRectangle		*area, 
	   GtkWidget		*widget, 
	   gint			 x, 
	   gint			 y, 
	   gint			 width, 
	   gint			 height,
	   gboolean		 fill)
{
	cairo_t		*cr;
	ccss_style_t	*style = NULL;
	char		*send_expose = NULL;

	g_return_if_fail (self);
	g_return_if_fail (self->stylesheet);

	if (node) {
		style = ccss_stylesheet_query (self->stylesheet, (ccss_node_t *) node);
	} else if (self->selector) {
		style = ccss_stylesheet_query_type (self->stylesheet, self->selector);
	}

	if (style) {
		cr = gdk_cairo_create (window);

		if (area) {
			gdk_cairo_rectangle (cr, area);
			cairo_clip (cr);
		}

		/* sanitise */
		if (width == -1 || height == -1) {
			gint w, h;
			gdk_drawable_get_size (GDK_DRAWABLE (window), &w, &h);
			width = width == -1 ? w : width;
			height = height == -1 ? h : height;
		}

		/* FIXME: force 'background: none' by inserting/frobbing? */
		ccss_cairo_style_draw_rectangle (style, cr, x, y, width, height);

		/* Handle custom expose hooks, this is where it gets dirty. */
		if (ccss_style_get_string (style, "gce-send-expose", &send_expose) &&
		    0 == g_strcmp0 (send_expose, "toplevel-focus-change")) {

			GtkWidget *toplevel = NULL;

			/* Check if the widget is already hooked up. */
			if (!g_hash_table_lookup (_widget_aux_hash, widget)) {
				toplevel = gtk_widget_get_toplevel (widget);
			}
			if (toplevel &&
			    GTK_WIDGET_TOPLEVEL (toplevel)) {

				gulong id;
				id = g_signal_connect (toplevel,
						       "notify::has-toplevel-focus",
						       G_CALLBACK (has_toplevel_focus_notify),
						       widget);
				/* FIXME: only one hook per widget this way.
				 * If we need more we can insert a struct with
				 * the IDs as value. */
				g_hash_table_insert (_widget_aux_hash,
						     widget,
						     GUINT_TO_POINTER (id));
				g_signal_connect (widget,
						  "destroy",
						  G_CALLBACK (widget_destroy_cb),
						  toplevel);
			}
		}

		cairo_destroy (cr), cr = NULL;
		ccss_style_destroy (style), style = NULL;
	}
}

static void
gap (GceStyle		*self,
     ccss_node_t	*node,
     GdkWindow		*window, 
     GdkRectangle	*area, 
     GtkWidget		*widget, 
     gint		 x, 
     gint		 y, 
     gint		 width, 
     gint		 height,
     GtkPositionType	 gap_side,
     gint		 gap_start,
     gint		 gap_width)
{
	cairo_t		*cr;
	ccss_style_t	*style = NULL;

	g_return_if_fail (self);
	g_return_if_fail (self->stylesheet);

	if (node) {
		style = ccss_stylesheet_query (self->stylesheet, (ccss_node_t *) node);
	} else if (self->selector) {
		style = ccss_stylesheet_query_type (self->stylesheet, self->selector);
	}

	if (style) {
		cr = gdk_cairo_create (window);

		if (area) {
			gdk_cairo_rectangle (cr, area);
			cairo_clip (cr);
		}

		/* sanitise */
		if (width == -1 || height == -1) {
			gint w, h;
			gdk_drawable_get_size (GDK_DRAWABLE (window), &w, &h);
			width = width == -1 ? w : width;
			height = height == -1 ? h : height;
		}

		ccss_cairo_style_draw_rectangle_with_gap (style, cr, x, y,
							  width, height,
							  gap_side, gap_start, gap_width);

		cairo_destroy (cr), cr = NULL;
		ccss_style_destroy (style), style = NULL;
	}
}

static void 
draw_hline (GtkStyle		*self, 
	    GdkWindow		*window, 
	    GtkStateType	 state, 
	    GdkRectangle	*area, 
	    GtkWidget		*widget, 
	    char const		*detail, 
	    gint		 x1, 
	    gint		 x2, 
	    gint		 y)
{
	char const	*role;
	ccss_node_t	*node = NULL;
	gint		 width = x2 - x1 + 1;
	gint		 height = 2;

	detail = gce_maps_get_detail (detail);
	role = gce_maps_match_role (widget, "hline", -1, detail,
				    x1, y, width, height);

	if (widget)
		node = gce_node_cache_fetch_node (widget, 
			detail,
			gce_maps_get_state (state),
			NULL, NULL, NULL, NULL, NULL,
			role ? role : "hline");

	/* FIXME: hardcode line width of 2, learn about wide separators. */
	rectangle (GCE_STYLE (self), node, window, area, widget,
		   x1, y, width, height, TRUE);

	gce_node_cache_release_node (node), node = NULL;
}

static void 
draw_vline (GtkStyle		*self, 
	    GdkWindow		*window, 
	    GtkStateType	 state, 
	    GdkRectangle	*area, 
	    GtkWidget		*widget, 
	    char const		*detail, 
	    gint		 y1, 
	    gint		 y2, 
	    gint		 x)
{
	char const	*role;
	ccss_node_t	*node = NULL;
	gint		 width = 2;
	gint		 height = y2 - y1 + 1;

	detail = gce_maps_get_detail (detail);
	role = gce_maps_match_role (widget, "vline", -1, detail,
				    x, y1, width, height);

	if (widget)
		node = gce_node_cache_fetch_node (widget, 
			detail,
			gce_maps_get_state (state),
			NULL, NULL, NULL, NULL, NULL,
			role ? role : "vline");

	/* FIXME: hardcode line width of 2, learn about wide separators. */
	rectangle (GCE_STYLE (self), node, window, area, widget,
		   x, y1, width, height, TRUE);

	gce_node_cache_release_node (node), node = NULL;

}

static void 
draw_shadow (GtkStyle		*self, 
	     GdkWindow		*window, 
	     GtkStateType	 state, 
	     GtkShadowType	 shadow, 
	     GdkRectangle	*area, 
	     GtkWidget		*widget, 
	     char const		*detail, 
	     gint		 x, 
	     gint		 y, 
	     gint		 width, 
	     gint		 height)
{
	char const	*role;
	ccss_node_t	*node = NULL;

	detail = gce_maps_get_detail (detail);
	role = gce_maps_match_role (widget, "shadow", shadow, detail, x, y, width, height);

	if (widget)
		node = gce_node_cache_fetch_node (widget, 
			detail,
			gce_maps_get_state (state),
			gce_maps_get_shadow (shadow),
			NULL, NULL, NULL, NULL,
			role ? role : "shadow");

	rectangle (GCE_STYLE (self), node, window, area, widget,
		   x, y, width, height, TRUE);

	gce_node_cache_release_node (node), node = NULL;
}

static void 
draw_box (GtkStyle		*self, 
	  GdkWindow		*window, 
	  GtkStateType		 state, 
	  GtkShadowType		 shadow,
	  GdkRectangle		*area,
	  GtkWidget		*widget,
	  char const		*detail,
	  gint			 x,
	  gint			 y,
	  gint			 width,
	  gint			 height)
{
	char const	*role;
	ccss_node_t	*node = NULL;

	detail = gce_maps_get_detail (detail);
	role = gce_maps_match_role (widget, "box", shadow, detail, x, y, width, height);

	if (widget)
		node = gce_node_cache_fetch_node (widget, 
			detail,
			gce_maps_get_state (state),
			gce_maps_get_shadow (shadow),
			NULL, NULL, NULL, NULL,
			role ? role : "box");

	rectangle (GCE_STYLE (self), node, window, area, widget,
		   x, y, width, height, TRUE);

	gce_node_cache_release_node (node), node = NULL;
}

static void 
draw_flat_box (GtkStyle		*self, 
	       GdkWindow	*window, 
	       GtkStateType	 state, 
	       GtkShadowType	 shadow,
	       GdkRectangle	*area,
	       GtkWidget	*widget,
	       char const	*detail,
	       gint		 x,
	       gint		 y,
	       gint		 width,
	       gint		 height)
{
	char const	*role;
	ccss_node_t	*node = NULL;

	detail = gce_maps_get_detail (detail);
	role = gce_maps_match_role (widget, "flat-box", shadow, detail, x, y, width, height);

	if (widget)
		node = gce_node_cache_fetch_node (widget, 
			detail,
			gce_maps_get_state (state),
			gce_maps_get_shadow (shadow),
			NULL, NULL, NULL, NULL,
			role ? role : "flat-box");

	rectangle (GCE_STYLE (self), node, window, area, widget, 
		   x, y, width, height, TRUE);

	gce_node_cache_release_node (node), node = NULL;
}

static void 
draw_check (GtkStyle		*self,
	    GdkWindow		*window,
	    GtkStateType	 state,
	    GtkShadowType	 shadow,
	    GdkRectangle	*area,
	    GtkWidget		*widget,
	    char const		*detail,
	    gint		 x,
	    gint		 y,
	    gint		 width,
	    gint		 height)
{
	char const	*role;
	ccss_node_t	*node = NULL;

	detail = gce_maps_get_detail (detail);
	role = gce_maps_match_role (widget, "check", shadow, detail, x, y, width, height);

	if (widget)
		node = gce_node_cache_fetch_node (widget, 
			detail,
			gce_maps_get_state (state),
			gce_maps_get_shadow (shadow),
			NULL, NULL, NULL, NULL,
			role ? role : "check");

	rectangle (GCE_STYLE (self), node, window, area, widget, 
		   x, y, width, height, TRUE);

	gce_node_cache_release_node (node), node = NULL;
}

static void 
draw_option (GtkStyle		 *self,
	     GdkWindow		 *window,
	     GtkStateType	 state,
	     GtkShadowType	 shadow,
	     GdkRectangle	 *area,
	     GtkWidget		 *widget,
	     char const		 *detail,
	     gint		 x,
	     gint		 y,
	     gint		 width,
	     gint		 height)
{
	char const	*role;
	ccss_node_t	*node = NULL;

	detail = gce_maps_get_detail (detail);
	role = gce_maps_match_role (widget, "option", shadow, detail, x, y, width, height);

	if (widget)
		node = gce_node_cache_fetch_node (widget, 
			detail,
			gce_maps_get_state (state),
			gce_maps_get_shadow (shadow),
			NULL, NULL, NULL, NULL,
			role ? role : "option");

	rectangle (GCE_STYLE (self), node, window, area, widget, 
		   x, y, width, height, TRUE);

	gce_node_cache_release_node (node), node = NULL;
}

static void 
draw_shadow_gap (GtkStyle		*self,
		 GdkWindow		*window,
		 GtkStateType		 state,
		 GtkShadowType		 shadow,
		 GdkRectangle		*area,
		 GtkWidget		*widget,
		 char const		*detail,
		 gint			 x,
		 gint			 y,
		 gint			 width,
		 gint			 height,
		 GtkPositionType	 gap_side,
		 gint			 gap_start,
		 gint			 gap_width)
{
	char const	*role;
	ccss_node_t	*node = NULL;

	detail = gce_maps_get_detail (detail);
	role = gce_maps_match_role (widget, "shadow-gap", shadow, detail, x, y, width, height);

	if (widget)
		node = gce_node_cache_fetch_node (widget, 
			detail,
			gce_maps_get_state (state),
			gce_maps_get_shadow (shadow),
			NULL, gce_maps_get_position (gap_side), NULL, NULL,
			role ? role : "shadow");

	gap (GCE_STYLE (self), node, window, area, widget, 
	     x, y, width, height, gap_side, gap_start, gap_width);

	gce_node_cache_release_node (node), node = NULL;
}

static void 
draw_box_gap (GtkStyle			*self,
	      GdkWindow			*window,
	      GtkStateType		 state,
	      GtkShadowType		 shadow,
	      GdkRectangle		*area,
	      GtkWidget			*widget,
	      char const		*detail,
	      gint			 x,
	      gint			 y,
	      gint			 width,
	      gint			 height,
	      GtkPositionType		 gap_side,
	      gint			 gap_start,
	      gint			 gap_width)
{
	char const	*role;
	ccss_node_t	*node = NULL;

	detail = gce_maps_get_detail (detail);
	role = gce_maps_match_role (widget, "box-gap", shadow, detail, x, y, width, height);

	if (widget)
		node = gce_node_cache_fetch_node (widget, 
			detail,
			gce_maps_get_state (state),
			gce_maps_get_shadow (shadow),
			NULL, gce_maps_get_position (gap_side), NULL, NULL,
			role ? role : "box");

	gap (GCE_STYLE (self), node, window, area, widget, 
	     x, y, width, height, gap_side, gap_start, gap_width);

	gce_node_cache_release_node (node), node = NULL;
}

static void 
draw_extension (GtkStyle	*self,
		GdkWindow	*window,
		GtkStateType	 state,
		GtkShadowType	 shadow,
		GdkRectangle	*area,
		GtkWidget	*widget,
		char const	*detail,
		gint		 x,
		gint		 y,
		gint		 width,
		gint		 height,
		GtkPositionType	 gap_side)
{
	char const	*role;
	gint		 gap_width;
	ccss_node_t	*node = NULL;

	detail = gce_maps_get_detail (detail);
	role = gce_maps_match_role (widget, "extension", shadow, detail, x, y, width, height);

	if (widget)
		node = gce_node_cache_fetch_node (widget, 
			detail,
			gce_maps_get_state (state),
			gce_maps_get_shadow (shadow),
			NULL, gce_maps_get_position (gap_side), NULL, NULL,
			role ? role : "extension");

	switch (gap_side) {
	case GTK_POS_LEFT:
	case GTK_POS_RIGHT:
		gap_width = height;
		break;
	case GTK_POS_TOP:
	case GTK_POS_BOTTOM:
		gap_width = width;
		break;
	}

	gap (GCE_STYLE (self), node, window, area, widget, 
	     x, y, width, height, gap_side, 0, gap_width);

	gce_node_cache_release_node (node), node = NULL;
}

static void 
draw_slider (GtkStyle		*self,
	     GdkWindow		*window,
	     GtkStateType	 state,
	     GtkShadowType	 shadow,
	     GdkRectangle	*area,
	     GtkWidget		*widget,
	     char const		*detail,
	     gint		 x,
	     gint		 y,
	     gint		 width,
	     gint		 height,
	     GtkOrientation	 orientation)
{
	char const	*role;
	ccss_node_t	*node = NULL;

	detail = gce_maps_get_detail (detail);
	role = gce_maps_match_role (widget, "slider", shadow, detail, x, y, width, height);

	if (widget)
		node = gce_node_cache_fetch_node (widget,
			detail,
			gce_maps_get_state (state),
			gce_maps_get_shadow (shadow),
			gce_maps_get_orientation (orientation),
			NULL, NULL, NULL,
			role ? role : "slider");

	rectangle (GCE_STYLE (self), node, window, area, widget, 
		   x, y, width, height, TRUE);

	gce_node_cache_release_node (node), node = NULL;
}

static void 
draw_handle (GtkStyle		*self,
	     GdkWindow		*window,
	     GtkStateType	 state,
	     GtkShadowType	 shadow,
	     GdkRectangle	*area,
	     GtkWidget		*widget,
	     char const		*detail,
	     gint		 x,
	     gint		 y,
	     gint		 width,
	     gint		 height,
	     GtkOrientation	 orientation)
{
	char const	*role;
	ccss_node_t	*node = NULL;

	detail = gce_maps_get_detail (detail);
	role = gce_maps_match_role (widget, "handle", shadow, detail, x, y, width, height);

	if (widget)
		node = gce_node_cache_fetch_node (widget,
			detail,
			gce_maps_get_state (state),
			gce_maps_get_shadow (shadow),
			gce_maps_get_orientation (orientation),
			NULL, NULL, NULL,
			role ? role : "handle");

	rectangle (GCE_STYLE (self), node, window, area, widget, 
		   x, y, width, height, TRUE);

	gce_node_cache_release_node (node), node = NULL;
}

static void 
draw_resize_grip (GtkStyle		*self,
		  GdkWindow		*window,
		  GtkStateType		 state,
		  GdkRectangle		*area,
		  GtkWidget		*widget,
		  char const		*detail,
		  GdkWindowEdge		 edge,
		  gint			 x,
		  gint			 y,
		  gint			 width,
		  gint			 height)
{
	char const	*role;
	ccss_node_t	*node = NULL;

	detail = gce_maps_get_detail (detail);
	role = gce_maps_match_role (widget, "resize-grip", -1, detail, x, y, width, height);

	if (widget)
		node = gce_node_cache_fetch_node (widget,
			detail,
			gce_maps_get_state (state),
			NULL, NULL, NULL, gce_maps_get_window_edge (edge), NULL,
			role ? role : "resizegrip");

	rectangle (GCE_STYLE (self), node, window, area, widget, 
		   x, y, width, height, TRUE);

	gce_node_cache_release_node (node), node = NULL;
}

static void 
draw_focus (GtkStyle		*self,
	    GdkWindow		*window,
	    GtkStateType	 state,
	    GdkRectangle	*area,
	    GtkWidget		*widget,
	    char const		*detail,
	    gint		 x,
	    gint		 y,
	    gint		 width,
	    gint		 height)
{
	char const	*role;
	ccss_node_t	*node = NULL;

	detail = gce_maps_get_detail (detail);
	role = gce_maps_match_role (widget, "focus", -1, detail, x, y, width, height);

	if (widget)
		node = gce_node_cache_fetch_node (widget,
			detail,
			gce_maps_get_state (state),
			NULL, NULL, NULL, NULL, NULL,
			role ? role : "focus");

	rectangle (GCE_STYLE (self), node, window, area, widget, 
		   x, y, width, height, FALSE);

	gce_node_cache_release_node (node), node = NULL;
}

#if 0
static void
draw_expander (GtkStyle		*self,
	       GdkWindow	*window,
	       GtkStateType	 state,
	       GdkRectangle	*area,
	       GtkWidget	*widget,
	       char const	*detail,
	       gint		 x,
	       gint		 y,
	       GtkExpanderStyle	 style)
{
#define DEFAULT_EXPANDER_SIZE 12
	char const	*role;
	gint		 expander_size;
	ccss_node_t	*node = NULL;

	detail = gce_maps_get_detail (detail);
	role = gce_maps_match_role (widget, "expander", -1, detail, x, y, -1, -1);

	if (widget &&
	    gtk_widget_class_find_style_property (GTK_WIDGET_GET_CLASS (widget),
						  "expander-size")) {
		gtk_widget_style_get (widget,
				      "expander-size", &expander_size,
				      NULL);
	} else {
		expander_size = DEFAULT_EXPANDER_SIZE;
	}

	if (widget)
		node = gce_node_cache_fetch_node (widget,
			detail,
			gce_maps_get_state (state),
			NULL, NULL, NULL, NULL, gce_maps_get_expander_style (style),
			role ? role : "expander");

	rectangle (GCE_STYLE (self), node, window, area, widget, 
		   x, y, expander_size, expander_size, TRUE);

	gce_node_cache_release_node (node), node = NULL;
#undef DEFAULT_EXPANDER_SIZE
}
#endif

static void
draw_diamond (GtkStyle		*self,
	      GdkWindow		*window,
	      GtkStateType	 state,
	      GtkShadowType	 shadow,
	      GdkRectangle	*area,
	      GtkWidget		*widget,
	      char const	*detail,
	      gint		 x,
	      gint		 y,
	      gint		 width,
	      gint		 height)
{
	char const	*role;
	ccss_node_t	*node = NULL;

	detail = gce_maps_get_detail (detail);
	role = gce_maps_match_role (widget, "diamond", shadow, detail, x, y, width, height);

	if (widget)
		node = gce_node_cache_fetch_node (widget,
			detail,
			gce_maps_get_state (state),
			gce_maps_get_shadow (shadow),
			NULL, NULL, NULL, NULL,
			role ? role : "diamond");

	rectangle (GCE_STYLE (self), node, window, area, widget, 
		   x, y, width, height, TRUE);

	gce_node_cache_release_node (node), node = NULL;
}

/*
 * TODO: handle `fill'?
 */
static void
draw_arrow (GtkStyle		*self,
	    GdkWindow		*window,
	    GtkStateType	 state,
	    GtkShadowType	 shadow,
	    GdkRectangle	*area,
	    GtkWidget		*widget,
	    char const		*detail,
	    GtkArrowType	 arrow,
	    gboolean		 fill,
	    gint		 x,
	    gint		 y,
	    gint		 width,
	    gint		 height)
{
	char const	*role;
	ccss_node_t	*node = NULL;

	detail = gce_maps_get_detail (detail);
	role = gce_maps_match_role (widget, "arrow", shadow, detail, x, y, width, height);

	if (widget)
		node = gce_node_cache_fetch_node (widget,
			detail,
			gce_maps_get_state (state),
			gce_maps_get_shadow (shadow),
			gce_maps_get_arrow (arrow),
			NULL, NULL, NULL,
			role ? role : "arrow");

	rectangle (GCE_STYLE (self), node, window, area, widget, 
		   x, y, width, height, TRUE);

	gce_node_cache_release_node (node), node = NULL;
}

static void
draw_tab (GtkStyle		*self,
	  GdkWindow		*window,
	  GtkStateType		 state,
	  GtkShadowType		 shadow,
	  GdkRectangle		*area,
	  GtkWidget		*widget,
	  char const		*detail,
	  gint			 x,
	  gint			 y,
	  gint			 width,
	  gint			 height)
{
	char const	*role;
	ccss_node_t	*node = NULL;

	detail = gce_maps_get_detail (detail);
	role = gce_maps_match_role (widget, "tab", shadow, detail, x, y, width, height);

	if (widget)
		node = gce_node_cache_fetch_node (widget,
			detail,
			gce_maps_get_state (state),
			gce_maps_get_shadow (shadow),
			NULL, NULL, NULL, NULL,
			role ? role : "tab");

	rectangle (GCE_STYLE (self), node, window, area, widget, 
		   x, y, width, height, TRUE);

	gce_node_cache_release_node (node), node = NULL;
}

static void
finalize (GObject *instance)
{
	GceStyle *self;

	self = GCE_STYLE (instance);

	if (self->stylesheet) {
		ccss_stylesheet_destroy (self->stylesheet);
		self->stylesheet = NULL;
	}

	if (self->selector) {
		g_free (self->selector);
		self->selector = NULL;
	}

	g_hash_table_unref (_widget_aux_hash);

	G_OBJECT_CLASS (gce_style_parent_class)->finalize (instance);
}

static void 
instance_init (GceStyle *self)
{
	self->stylesheet = NULL;
	self->selector = NULL;

	if (_widget_aux_hash == NULL) {
		_widget_aux_hash = g_hash_table_new (g_direct_hash, g_direct_equal);
	} else {
		g_hash_table_ref (_widget_aux_hash);
	}
}

static void 
class_init (GceStyleClass *klass)
{
	GObjectClass	*go_class;
	GtkStyleClass	*style_class;

	gce_style_parent_class = g_type_class_peek_parent (klass);

	go_class = G_OBJECT_CLASS (klass);
	go_class->finalize = finalize;

	style_class = GTK_STYLE_CLASS (klass);
	style_class->draw_hline = draw_hline;
	style_class->draw_vline = draw_vline;
	style_class->draw_shadow = draw_shadow;
	/*
	draw_polygon
	*/
	style_class->draw_arrow = draw_arrow;
	style_class->draw_diamond = draw_diamond;
	style_class->draw_box = draw_box;
	style_class->draw_flat_box = draw_flat_box;
	style_class->draw_check = draw_check;
	style_class->draw_option = draw_option;
	style_class->draw_tab = draw_tab;
	style_class->draw_shadow_gap = draw_shadow_gap;
	style_class->draw_box_gap = draw_box_gap;
	style_class->draw_extension = draw_extension;
	style_class->draw_focus = draw_focus;
	style_class->draw_slider = draw_slider;
	style_class->draw_handle = draw_handle;
	/* FIXME: do own expander sometimes.
	style_class->draw_expander = draw_expander;
	*/
	/*
	draw_layout
	*/
	style_class->draw_resize_grip = draw_resize_grip;
}

void
gce_style_register_type (GTypeModule *module)
{
	if (!gce_style_type) {
		static const GTypeInfo info = {
			sizeof (GceStyleClass),
			NULL,
			NULL,
			(GClassInitFunc) class_init,
			NULL,                   /* class_finalize */
			NULL,                   /* class_data */
			sizeof (GceStyle),
			0,                      /* n_preallocs */
			(GInstanceInitFunc) instance_init,
		};

		gce_style_type = g_type_module_register_type (module, 
							       GTK_TYPE_STYLE, 
							       "GceStyle", 
							       &info, 0);
	}
}

GType
gce_style_get_type (void) 
{
	return gce_style_type;
}
