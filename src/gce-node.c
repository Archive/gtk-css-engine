/* vim: set ts=8 sw=8 noexpandtab: */

/* The CSS Theme Engine for Gtk+.
 * Copyright (C) 2008 Robert Staudinger
 *
 * This  library is free  software; you can  redistribute it and/or
 * modify it  under  the terms  of the  GNU Lesser  General  Public
 * License  as published  by the Free  Software  Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed  in the hope that it will be useful,
 * but  WITHOUT ANY WARRANTY; without even  the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License  along  with  this library;  if not,  write to  the Free
 * Software Foundation, Inc., 51  Franklin St, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 */

#include <string.h>
#include <ccss-gtk/ccss-gtk.h>
#include "gce-maps.h"
#include "gce-node.h"
#include "config.h"

typedef struct {
	GtkWidget		*widget;
	struct GceNodeAttribs {
		char const	*classes[2];
		char const	*pseudo_classes[2];
		char const	*gap;
		char const	*shadow;
		char const	*orientation;
		char const	*edge;
		char const	*expander_style;
	} attribs;
	enum GceNodeFlavor {
		UNSET,
		CONTAINER,
		PRIMITIVE,
		TYPE 
	} flavor;
	union {
		char const	*primitive;
		GType		 gtype;
	} impl;
} GceNode;

static ccss_node_t * create_widget_container_node       (GtkWidget		*widget);
static ccss_node_t * create_widget_node			(GtkWidget		*widget,
							 struct GceNodeAttribs   attribs);
static ccss_node_t * create_widget_base_node		(GtkWidget		*widget,
							 enum GceNodeFlavor	 flavor,
							 GType			 gtype);

static bool
is_a (ccss_node_t const	*self,
      char const	*type_name)
{
	GceNode const   *node = ccss_node_get_user_data (self);
	GType		 gtype;
	bool		 is_matching;

	switch (node->flavor) {
	case PRIMITIVE:
		if (0 == strcmp (type_name, node->impl.primitive)) {
			return TRUE;
		}
		/* fall thru */
	case UNSET:
	case CONTAINER:
	case TYPE:
		gtype = g_type_from_name (type_name);
		is_matching = g_type_is_a (G_OBJECT_TYPE (node->widget), gtype);
		return is_matching;
	default:
		g_warning ("Unknown flavor %d\n", node->flavor);
		g_assert_not_reached ();
		return FALSE;
	}
}

static ccss_node_t *
get_container (ccss_node_t const *self)
{
	GceNode const   *node = ccss_node_get_user_data (self);
	GtkWidget	*container;
	ccss_node_t	*container_node;

	g_assert (node);

	container_node = NULL;
	switch (node->flavor) {
	case UNSET:
	case CONTAINER:
		container = gtk_widget_get_parent (node->widget);
		if (container) {
			container_node = create_widget_container_node (container);
		}
		break;
	case PRIMITIVE:
		/* Properties propagate to the primitive's widget. */
		container_node = create_widget_node (node->widget, node->attribs);
		break;
	case TYPE:
		g_assert_not_reached ();
		/* fall thru */
	default:
		container_node = NULL;
	}

	return container_node;
}

static ccss_node_t *
get_base_style (ccss_node_t const *self)
{
	GceNode const   *node = ccss_node_get_user_data (self);
	ccss_node_t	*base_node;

	g_assert (node);

	switch (node->flavor) {
	case UNSET:
		if (GTK_TYPE_WIDGET == G_OBJECT_TYPE (G_OBJECT (node->widget))) {
			/* already at top of inheritance hierarchy */
			base_node = NULL;
		} else {
			/* inherit from superclass widget */
			base_node = create_widget_base_node (node->widget,
							     TYPE,
							     g_type_parent (G_OBJECT_TYPE (node->widget)));
		}
		break;
	case CONTAINER:
		if (GTK_TYPE_WIDGET == node->impl.gtype) {
			/* already at top of inheritance hierarchy */
			base_node = NULL;
		} else {
			/* inherit from superclass widget */
			base_node = create_widget_base_node (node->widget,
							     CONTAINER,
							     g_type_parent (node->impl.gtype));
		} 
		break;
	case PRIMITIVE:
		base_node = NULL;
		break;
	case TYPE:
		if (GTK_TYPE_WIDGET == node->impl.gtype) {
			/* already at top of inheritance hierarchy */
			base_node = NULL;
		} else {
			/* inherit from superclass widget */
			base_node = create_widget_base_node (node->widget,
							     TYPE,
							     g_type_parent (node->impl.gtype));
		} 
		break;
	default:
		g_assert_not_reached ();
		base_node = NULL;
	}

	return base_node;
}

static char const *
get_id (ccss_node_t const *self)
{
	GceNode const   *node = ccss_node_get_user_data (self);

	g_return_val_if_fail (node, NULL);

	switch (node->flavor) {
	case UNSET:
	case CONTAINER:
	case PRIMITIVE:
		return gtk_widget_get_name (node->widget);
	case TYPE:
	default:
		g_assert_not_reached ();
		return NULL;
	}
}

static char const *
get_type (ccss_node_t const *self)
{
	GceNode const   *node = ccss_node_get_user_data (self);

	g_return_val_if_fail (node, NULL);

	switch (node->flavor) {
	case UNSET:
		return G_OBJECT_TYPE_NAME (G_OBJECT (node->widget));
	case CONTAINER:
	case TYPE:
		return g_type_name (node->impl.gtype);
	case PRIMITIVE:
		g_assert (node->impl.primitive);
		return node->impl.primitive;
	default:
		g_assert_not_reached ();
		return NULL;
	}
}

static char const **
get_classes (ccss_node_t const *self)
{
	GceNode const   *node = ccss_node_get_user_data (self);

	g_return_val_if_fail (node, NULL);

	return (char const **) node->attribs.classes;
}

static char const **
get_pseudo_classes (ccss_node_t const *self)
{
	GceNode const   *node = ccss_node_get_user_data (self);

	g_return_val_if_fail (node, NULL);

	return (char const **) node->attribs.pseudo_classes;
}

static char *
_to_string (GValue const *value)
{
	GType	type;
	gint	enum_value;

	type = G_VALUE_TYPE (value);

	switch (type) {
	case G_TYPE_BOOLEAN:
		return g_value_get_boolean (value) ?
				g_strdup ("true") : 
				g_strdup ("false");
	case G_TYPE_CHAR:
		return g_strdup_printf ("%c", g_value_get_char (value));
	case G_TYPE_UCHAR:
		return g_strdup_printf ("%c", g_value_get_uchar (value));
	case G_TYPE_INT:
		return g_strdup_printf ("%d", g_value_get_int (value));
	case G_TYPE_UINT:
		return g_strdup_printf ("%u", g_value_get_uint (value));
	case G_TYPE_LONG:
		return g_strdup_printf ("%ld", g_value_get_long (value));
	case G_TYPE_ULONG:
		return g_strdup_printf ("%ld", g_value_get_ulong (value));
/* Not supported by C99. TODO: enable after splitting out libccss.
	case G_TYPE_INT64:
		return g_strdup_printf ("%Ld", g_value_get_int64 (value));
	case G_TYPE_UINT64:
		return g_strdup_printf ("%Ld", g_value_get_uint64 (value));
*/
	case G_TYPE_FLOAT:
		return g_strdup_printf ("%.03f", g_value_get_float (value));
	case G_TYPE_DOUBLE:
		return g_strdup_printf ("%.03f", g_value_get_double (value));
	case G_TYPE_STRING:
		return g_strdup (g_value_get_string (value));
	default:
		/* Fall thru. Need some dummy code. */
		enum_value = 0;
	}

	/* Non-fundamental types can not be handled using `switch'. */
	if (GTK_TYPE_ORIENTATION == type) {
		enum_value = g_value_get_enum (value);
		return g_strdup (gce_maps_get_orientation ((GtkPositionType) enum_value));
	} else if (GTK_TYPE_POSITION_TYPE == type) {
		enum_value = g_value_get_enum (value);
		return g_strdup (gce_maps_get_position ((GtkPositionType) enum_value));
	}

	g_warning ("Could not convert `%s'", g_type_name (type));
	return NULL;
}

static char *
get_attribute (ccss_node_t const	*self,
	       char const		*name)
{
	GceNode const   *node = ccss_node_get_user_data (self);
	GParamSpec	*param;
	GValue		 property = { 0, };

	g_assert (node && node->widget);

	if (node->flavor == TYPE) {
		g_warning ("Querying attribute `%s' on something not a widget", name);
		return NULL;
	}

	/* Attributes provided in the drawing function take precedence. */
	if (node->flavor == PRIMITIVE) {
		if (0 == strcmp ("shadow", name)) {
			return g_strdup (node->attribs.shadow);
		} else if (0 == strcmp ("orientation", name)) {
			return g_strdup (node->attribs.orientation);
		} else if (0 == strcmp ("edge", name)) {
			return g_strdup (node->attribs.edge);
		} else if (0 == strcmp ("expander-style", name)) {
			return g_strdup (node->attribs.expander_style);
		} else if (0 == strcmp ("gap", name)) {
			return g_strdup (node->attribs.gap);
		}
	}

	/* Now try to find an apropriate style property. */
	param = gtk_widget_class_find_style_property (
			GTK_WIDGET_CLASS (G_OBJECT_GET_CLASS (node->widget)),
			name);
	if (param) {
		g_value_init (&property, param->value_type);
		gtk_widget_style_get_property (node->widget, name, &property);
		return _to_string (&property);
	}

	/* Next look for an apropriate gobject property. */
	param = g_object_class_find_property (
			G_OBJECT_GET_CLASS (node->widget),
			name);
	if (param) {
		g_value_init (&property, param->value_type);
		g_object_get_property (G_OBJECT (node->widget), name, &property);
		return _to_string (&property);
	}

	return NULL;
}

static bool
get_viewport (ccss_node_t const	*self,
	      double		*x,
	      double		*y,
	      double		*width,
	      double		*height)
{
	GceNode const   *node = ccss_node_get_user_data (self);

	switch (node->flavor) {
	case UNSET:
	case PRIMITIVE:
		*x = node->widget->allocation.x;
		*y = node->widget->allocation.y;
		*width = node->widget->allocation.width;
		*height = node->widget->allocation.height;
		break;
	case TYPE:
	case CONTAINER:
		/* Viewport is not valid. */
		return FALSE;
	}

	return TRUE;
}

static void
release (ccss_node_t *self)
{
	gce_node_cache_release_node (self);
}

static ccss_node_class_t _node_class = {
	.is_a			= is_a,
	.get_container		= get_container,
	.get_base_style		= get_base_style,
	.get_instance		= NULL,
	.get_id			= get_id,
	.get_type		= get_type,
	.get_classes		= get_classes,
	.get_pseudo_classes	= get_pseudo_classes,
	.get_attribute		= get_attribute,
	.get_style		= NULL,
	.get_viewport		= get_viewport,
	.release		= release
};

ccss_node_t *
gce_node_cache_fetch_node (GtkWidget	*widget,
			   char const	*class_name,
			   char const	*pseudo_class,
			   char const	*shadow,
			   char const	*orientation,
			   char const	*gap,
			   char const	*edge,
			   char const	*expander_style,
			   char const	*primitive)
{
	ccss_node_t     *self;
	GceNode		*node;

	node = g_new0 (GceNode, 1);
	node->widget = widget;
	node->attribs.classes[0] = class_name;
	node->attribs.classes[1] = NULL;
	node->attribs.pseudo_classes[0] = pseudo_class;
	node->attribs.pseudo_classes[1] = NULL;
	node->attribs.gap = gap;
	node->attribs.shadow = shadow;
	node->attribs.orientation = orientation;
	node->attribs.edge = edge;
	node->attribs.expander_style = expander_style;
	node->flavor = PRIMITIVE;
	node->impl.primitive = primitive;

	self = ccss_node_create (&_node_class,
				 CCSS_NODE_CLASS_N_METHODS (_node_class),
				 node);
	return self;
}

void
gce_node_cache_release_node (ccss_node_t *self)
{
	GceNode *node;

	node = ccss_node_get_user_data (self);
	g_free (node);

	ccss_node_destroy (self);
}

static ccss_node_t *
create_widget_container_node (GtkWidget *widget)
{
	ccss_node_t     *self;
	GceNode		*node;

	node = g_new0 (GceNode, 1);
	node->widget = widget;
	node->flavor = CONTAINER;
	node->impl.gtype = G_OBJECT_TYPE (widget);

	self = ccss_node_create (&_node_class,
				 CCSS_NODE_CLASS_N_METHODS (_node_class),
				 node);
	return self;
}

static ccss_node_t *
create_widget_node (GtkWidget			*widget,
		    struct GceNodeAttribs        attribs)
{
	ccss_node_t     *self;
	GceNode		*node;

	node = g_new0 (GceNode, 1);
	node->widget = widget;
	node->flavor = UNSET;
	node->attribs = attribs;

	self = ccss_node_create (&_node_class,
				 CCSS_NODE_CLASS_N_METHODS (_node_class),
				 node);
	return self;
}

static ccss_node_t *
create_widget_base_node (GtkWidget		*widget,
			 enum GceNodeFlavor	 flavor,
			 GType			 gtype)
{
	ccss_node_t     *self;
	GceNode		*node;

	node = g_new0 (GceNode, 1);
	node->widget = widget;
	node->flavor = flavor;
	node->impl.gtype = gtype;

	self = ccss_node_create (&_node_class,
				 CCSS_NODE_CLASS_N_METHODS (_node_class),
				 node);
	return self;
}

