/* vim: set ts=8 sw=8 noexpandtab: */

/* The CSS Theme Engine for Gtk+.
 * Copyright (C) 2008 Robert Staudinger
 *
 * This  library is free  software; you can  redistribute it and/or
 * modify it  under  the terms  of the  GNU Lesser  General  Public
 * License  as published  by the Free  Software  Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed  in the hope that it will be useful,
 * but  WITHOUT ANY WARRANTY; without even  the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License  along  with  this library;  if not,  write to  the Free
 * Software Foundation, Inc., 51  Franklin St, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 */

#include <gtk/gtk.h>
#include <string.h>
#include "gce-rc-style.h"
#include "gce-style.h"
#include "config.h"

struct _GceRcStyle {
	GtkRcStyle		 parent;
	ccss_stylesheet_t	*stylesheet;
	char			*selector;
};

struct _GceRcStyleClass {
	GtkRcStyleClass parent;
};

static GType            gce_rc_style_type = 0;
static GtkRcStyleClass *gce_rc_style_parent_class = NULL;

static GtkStyle *
create_style (GtkRcStyle *gtk_rc_style)
{
	GceRcStyle	*rc_style;
	GceStyle	*style;

	rc_style = GCE_RC_STYLE (gtk_rc_style);
	style = GCE_STYLE (g_object_new (GCE_TYPE_STYLE, NULL));

	if (rc_style && rc_style->stylesheet) {
		style->stylesheet = ccss_stylesheet_reference (rc_style->stylesheet);
	} else {
		g_warning ("%s() rc-style %p, stylesheet %p", __FUNCTION__,
			   (void *) rc_style,
			   rc_style ? (void *) rc_style->stylesheet : NULL);
	}

	style->selector = g_strdup (rc_style->selector);

	return GTK_STYLE (style);
}

/* FIXME: do more robust parsing. */
static void
parse_href (GtkRcStyle	*rc_style,
	    GtkSettings	*settings, 
	    GScanner	*scanner)
{
	ccss_grammar_t		*grammar;
	ccss_stylesheet_t	*stylesheet;
	char			*gce_file;
	guint			 token;
	guint			 descriptor;

	token = g_scanner_get_next_token (scanner);
	g_assert (token == '=');

	token = g_scanner_get_next_token (scanner);
	g_assert (token == G_TOKEN_STRING);

	/* TODO use default settings here, or the ones passed to GtkRcStyle::parse */
	gce_file = gtk_rc_find_pixmap_in_path (gtk_settings_get_default (),
			scanner, scanner->value.v_string);

	grammar = ccss_gtk_grammar_create ();
	stylesheet = ccss_grammar_create_stylesheet_from_file (grammar,
							gce_file, NULL);
	if (stylesheet) {
		ccss_grammar_destroy (grammar), grammar = NULL;
	} else {
		g_critical ("Could not create stylesheet `%s'", gce_file);
	}
	g_free (gce_file), gce_file = NULL;

	/* User-agent stylesheet */
	descriptor = ccss_stylesheet_add_from_file (stylesheet, GCE_UA_STYLESHEET,
						    CCSS_STYLESHEET_USER_AGENT,
						    NULL);
	if (descriptor) {

		GceRcStyle *gce_rc_style;

		gce_rc_style = GCE_RC_STYLE (rc_style);
		if (gce_rc_style) {
			gce_rc_style->stylesheet = ccss_stylesheet_reference (stylesheet);
		}

		if (g_getenv ("GCE_PREVIEW") != NULL) {
			char *rc_string;
			rc_string = ccss_gtk_stylesheet_to_gtkrc (stylesheet);
			if (rc_string) {
				gtk_rc_parse_string (rc_string);
				g_free (rc_string), rc_string = NULL;
			}
		}

		ccss_stylesheet_destroy (stylesheet), stylesheet = NULL;
	} else {
		g_critical ("Could not add user-agent stylesheet `%s'",
			    GCE_UA_STYLESHEET);
	}
}

/* FIXME: do more robust parsing. */
static void
parse_selector (GtkRcStyle	*rc_style,
		GtkSettings	*settings, 
		GScanner	*scanner)
{
	guint token;

	token = g_scanner_get_next_token (scanner);
	g_assert (token == '=');

	token = g_scanner_get_next_token (scanner);
	g_assert (token == G_TOKEN_STRING);

	GCE_RC_STYLE (rc_style)->selector = g_strdup (scanner->value.v_string);
}

/* FIXME: do more robust parsing. */
static guint 
parse (GtkRcStyle  *rc_style, 
       GtkSettings *settings, 
       GScanner    *scanner)
{
	static GQuark scope_id = 0;

	guint	 old_scope;
	guint	 token;
		
	if (!scope_id)
		scope_id = g_quark_from_string ("gce_engine");
	
	old_scope = g_scanner_set_scope (scanner, scope_id);
	
	token = g_scanner_peek_next_token (scanner);
	if (token != G_TOKEN_RIGHT_CURLY) {

		token = g_scanner_get_next_token (scanner);
		if (G_TOKEN_IDENTIFIER == token &&
		    0 == g_strcmp0 ("href", scanner->value.v_identifier)) {

			parse_href (rc_style, settings, scanner);

		} else if (G_TOKEN_IDENTIFIER == token &&
			   0 == g_strcmp0 ("selector", scanner->value.v_identifier)) {

			parse_selector (rc_style, settings, scanner);

		} else {
			g_warning ("unknown token `%d'", token);
		}
	}

	g_scanner_get_next_token (scanner);	
	g_scanner_set_scope (scanner, old_scope);

	return G_TOKEN_NONE;
}

static void 
merge (GtkRcStyle *dest, 
       GtkRcStyle *src)
{
	if (GCE_IS_RC_STYLE (dest) &&
	    GCE_IS_RC_STYLE (src)) {

		GceRcStyle *from;
		GceRcStyle *to;

		from = GCE_RC_STYLE (src);
		to = GCE_RC_STYLE (dest);

		if (from->stylesheet != NULL &&
		    to->stylesheet == NULL) {
			to->stylesheet = ccss_stylesheet_reference (from->stylesheet);
		}

		to->selector = g_strdup (from->selector);
	}

	gce_rc_style_parent_class->merge (dest, src);
}

static void
finalize (GObject *instance)
{
	GceRcStyle *self;

	self = GCE_RC_STYLE (instance);

	if (self->stylesheet) {
		ccss_stylesheet_destroy (self->stylesheet);
		self->stylesheet = NULL;
	}

	if (self->selector) {
		g_free (self->selector);
		self->selector = NULL;
	}

	G_OBJECT_CLASS (gce_rc_style_parent_class)->finalize (instance);
}

static void 
instance_init (GceRcStyle *self)
{
	self->stylesheet = NULL;
	self->selector = NULL;
}

static void 
class_init (GceRcStyleClass *klass)
{
	GObjectClass	*go_class;
	GtkRcStyleClass *rc_style_class;

	gce_rc_style_parent_class = g_type_class_peek_parent (klass);

	go_class = G_OBJECT_CLASS (klass);
	go_class->finalize = finalize;

	rc_style_class = GTK_RC_STYLE_CLASS (klass);
	rc_style_class->create_style = create_style;
	rc_style_class->parse = parse;
	rc_style_class->merge = merge;
}

void 
gce_rc_style_register_type (GTypeModule *module)
{
	if (!gce_rc_style_type) {
		static const GTypeInfo info = {
			sizeof(GceRcStyleClass),
			NULL,
			NULL,
			(GClassInitFunc) class_init,
			NULL,                   /* class_finalize */
			NULL,                   /* class_data */
			sizeof (GceRcStyle),
			0,                      /* n_preallocs */
			(GInstanceInitFunc) instance_init,
		};

		gce_rc_style_type = g_type_module_register_type (module, 
								  GTK_TYPE_RC_STYLE, 
								  "GceRcStyle", 
								  &info, 0);
	}
}

GType
gce_rc_style_get_type (void) 
{
	return gce_rc_style_type;
}
