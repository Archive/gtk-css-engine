
.. This document is written using reStructuredText, see http://docutils.sourceforge.net/rst.html .

====================
CSS Theming for Gtk+
====================

While the GNOME theming scene is thriving with exciting projects like Aurora, Clearlooks and Murrine, application developers and designers are increasingly concerned [#evil1]_ [#evil2]_ [#evil3]_ [#evil4]_ about the sustainability of the current concept involving a theme engine providing a set of styling options, and a gtkrc [#gtkrc]_ file specifying colours and fine-tuning engine-behaviour. A somewhat obvious alternative approach is to put all styling information into the configuration file and provide a single theme engine that is powerful enough to satisfy the designers' demand. This is where `CSS <http://www.w3.org/TR/CSS21/>`_ comes into play. CSS already works well making millions of web-pages look good, the *gtk-css-engine* project is about making a some of its features available for applications using the Gtk+ toolkit. However, an application window containing menus, toolbars, buttons and text entry fields is not exactly the same as a web-page, so it is worthwile discussing how to work around the impedance mismatch.

So what is required for Gtk+ to work with CSS? Let us first look at the elements that Gtk+ makes available for theming. At the lowest level there is a number of basic building blocks, the so-called *primitives* (arrow, horizontal line, vertical line, checkmark. etc. see `below <#Primitives>`_ for an exhaustive list). All widgets are drawn using those primitives, a GtkButton for example consists of a ``box`` element for border and background, and a ``layout`` element drawn on top of it, showing the button's text label. So much for theory, more about there relation between primitives and widgets in a minute. The following example shows how to do theme a simple button.
::

	GtkButton {
		background-color: red;
		border: 1px solid black;
	}

Theming a button is pretty straight forward, a scrollbar on the other hand 


Appendix
========

Primitives
----------

=====================================	============
``arrow``				``box``
``boxgap``, mapped onto ``box``		``check``
``diamond``				``expander``
``extension``				``flatbox``
``focus``				``handle``
``hline``				``layout``
``option``				``polygon``
``resizegrip``				``shadow``
``shadowgap``, mapped onto ``shadow``	``slider``
``tab``					``vline``
=====================================	============

C.f. [#gtkstyle]_, each primitive is drawn using a function named gtk_paint_<primitive>, exept for ``boxgap`` (see ``gtk_paint_box_gap``), ``flatbox`` (see ``gtk_paint_flat_box``), ``resizegrip`` (see ``gtk_paint_resize_grip``) and ``shadowgap`` (see ``gtk_paint_shadow_gap``).

The ``boxgap`` and ``shadowgap`` primitives are not directly available to CSS themes, the styling information for ``box`` and ``shadow`` is used respectively. They can still be matched using attributes, e.g. ``box[gap=top] { ... }``

References
----------

.. [#evil1] http://blogs.gnome.org/mortenw/2008/02/02/themes-are-evil-part-ii/
.. [#evil2] http://blogs.gnome.org/mortenw/2007/05/27/themes-are-evil/
.. [#evil3] http://abock.org/2007/07/02/suboptimal-theming-in-gtk/
.. [#evil4] http://jimmac.musichall.cz/log/?p=435
.. [#gtkrc] http://library.gnome.org/devel/gtk/stable/gtk-Resource-Files.html

.. [#gtkstyle] http://library.gnome.org/devel/gtk/stable/GtkStyle.html

.. [#qt-stylesheet] http://doc.trolltech.com/4.5/stylesheet.html
